package by.nerallan.financialrecords.ui.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import by.nerallan.financialrecords.R;

class CostViewHolder extends RecyclerView.ViewHolder {

	@Nullable
	private TextView descriptionTextView;
	@Nullable
	private TextView valueTextView;
	@Nullable
	private TextView timestampTextView;

	CostViewHolder(@NonNull View itemView) {
		super(itemView);
		this.descriptionTextView = itemView.findViewById(R.id.description_text_view);
		this.valueTextView = itemView.findViewById(R.id.value_text_view);
		this.timestampTextView = itemView.findViewById(R.id.date_text_view);
	}

	void bindCost(@Nullable String description, @Nullable String value, @Nullable String timestamp){
		if (descriptionTextView != null) {
			descriptionTextView.setText(description);
		}
		if (valueTextView != null) {
			valueTextView.setText(value);
		}
		if (timestampTextView != null) {
			timestampTextView.setText(timestamp);
		}
	}
}
