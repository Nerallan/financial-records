package by.nerallan.financialrecords.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import by.nerallan.financialrecords.ui.model.Cost;

public class RecyclerViewAdapter extends RecyclerView.Adapter<CostViewHolder>{

	private List<Cost> costList;
	private int itemLayout;

	public RecyclerViewAdapter(List<Cost> costList, int itemLayout) {
		this.costList = costList;
		this.itemLayout = itemLayout;
	}

	@NonNull
	@Override
	public CostViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(itemLayout, viewGroup, false);
		return new CostViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull CostViewHolder holder, int i) {
		Cost cost = costList.get(i);
		holder.bindCost(cost.getDescription(), String.valueOf(cost.getValue()), String.valueOf(cost.getTimestamp()));
	}

	@Override
	public int getItemCount() {
		return costList.size();
	}
}
