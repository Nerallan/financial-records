package by.nerallan.financialrecords.ui.view.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import by.nerallan.financialrecords.R;
import by.nerallan.financialrecords.ui.view.fragment.EditRecordFragment;
import by.nerallan.financialrecords.ui.view.fragment.ShowRecordsFragment;

public class MainActivity extends AppCompatActivity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Toolbar topToolbar = findViewById(R.id.activity_main_toolbar);
		topToolbar.setTitle(R.string.app_name);

		EditRecordFragment editRecordFragment = new EditRecordFragment().newInstance();
		ShowRecordsFragment showRecordsFragment = new ShowRecordsFragment().newInstance();

		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();
		transaction.add(R.id.edit_record_fragment, editRecordFragment);
		transaction.add(R.id.show_records_fragment, showRecordsFragment);
		transaction.commit();
	}
}
