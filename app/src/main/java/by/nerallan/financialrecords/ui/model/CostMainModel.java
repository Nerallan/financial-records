package by.nerallan.financialrecords.ui.model;

import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

import by.nerallan.financialrecords.ui.contract.MainContract;

public class CostMainModel implements MainContract.MainModel {

	@Override
	public void addItem(Cost cost) {

	}

	@Override
	public void removeItem(int id) {

	}

	@Override
	public void loadItems(final MainContract.MainPresenter.OnDataChangeListener listener){
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				listener.onSuccess(createCollection());
			}
		}, 2000);
	}

	private List<Cost> createCollection(){
		List<Cost> costList = new ArrayList<>();
		Cost cost1 = new Cost("first product", 120, 1447401007);
		Cost cost2 = new Cost("second product", 130, 1447401007);
		Cost cost3 = new Cost("third product", 140, 1447401007);
		Cost cost4 = new Cost("fouth product", 150, 1447401007);
		Cost cost5 = new Cost("fouth product", 150, 1447401007);
		Cost cost6 = new Cost("fouth product", 150, 1447401007);
		costList.add(cost1);
		costList.add(cost2);
		costList.add(cost3);
		costList.add(cost4);
		costList.add(cost5);
		costList.add(cost6);
		return costList;
	}


	@Override
	public Cost editItem(Cost cost) {
		return null;
	}
}
