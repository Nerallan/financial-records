package by.nerallan.financialrecords.ui.model;

public class Cost {
	private String description;
	private int value;
	private long timestamp;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public Cost(String description, int value, long timestamp) {
		this.description = description;
		this.value = value;
		this.timestamp = timestamp;
	}
}
