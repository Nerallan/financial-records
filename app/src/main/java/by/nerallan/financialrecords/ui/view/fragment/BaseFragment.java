package by.nerallan.financialrecords.ui.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import by.nerallan.financialrecords.R;
import by.nerallan.financialrecords.ui.contract.MainContract;
import by.nerallan.financialrecords.ui.model.Cost;
import by.nerallan.financialrecords.ui.presenter.CostPresenter;

public abstract class BaseFragment extends Fragment implements MainContract.MainView {

	private RecyclerView recyclerView;
	private ProgressBar progressBar;
	private MainContract.MainPresenter mainPresenter;
	private RecyclerView.Adapter adapter;

	@Nullable
	@Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(getLayout(), container, false);

		initViews(rootView);

		mainPresenter = new CostPresenter();
		mainPresenter.onAttachView(this);
		setupRecyclerView();

		return rootView;
	}

	@Override public void onResume() {
		super.onResume();
		mainPresenter.onResume();
	}

	private void initViews(View view) {
		recyclerView =  view.findViewById(R.id.recycler_view_main);
		progressBar = view.findViewById(R.id.progress_bar);
	}

	@Override
	public void setCollection(List<Cost> costList) {
		adapter = getAdapter(costList);
		recyclerView.setAdapter(adapter);
	}

	@Override
	public void setNoData(String message) {

	}

	@Override
	public void showProgress() {
		progressBar.setVisibility(View.VISIBLE);
		recyclerView.setVisibility(View.INVISIBLE);
	}

	@Override
	public void hideProgress() {
		progressBar.setVisibility(View.INVISIBLE);
		recyclerView.setVisibility(View.VISIBLE);
	}

	@Override public void onDestroy() {
		mainPresenter.onDetachView();
		super.onDestroy();
	}

	private void setupRecyclerView() {
		if(getLayoutManager() != null) {
			recyclerView.setLayoutManager(getLayoutManager());
		}
		recyclerView.setItemAnimator(new DefaultItemAnimator());
	}

	protected abstract int getLayout();

	protected abstract RecyclerView.LayoutManager getLayoutManager();

	protected abstract RecyclerView.Adapter getAdapter(List<Cost> costList);

}
