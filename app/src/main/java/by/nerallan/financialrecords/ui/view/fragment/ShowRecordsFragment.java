package by.nerallan.financialrecords.ui.view.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import by.nerallan.financialrecords.R;
import by.nerallan.financialrecords.ui.adapter.RecyclerViewAdapter;
import by.nerallan.financialrecords.ui.model.Cost;

public class ShowRecordsFragment extends BaseFragment {

	public ShowRecordsFragment newInstance() {
		return new ShowRecordsFragment();
	}

	@Override
	protected int getLayout() {
		return R.layout.fragment_recycler_main;
	}

	@Override
	protected RecyclerView.LayoutManager getLayoutManager() {
		return new LinearLayoutManager(getContext());
	}

	@Override
	protected RecyclerView.Adapter getAdapter(List<Cost> costList) {
		return new RecyclerViewAdapter(costList, R.layout.list_row);
	}
}
