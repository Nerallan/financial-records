package by.nerallan.financialrecords.ui.presenter;

import android.util.Log;

import java.util.List;

import by.nerallan.financialrecords.ui.contract.MainContract;
import by.nerallan.financialrecords.ui.model.Cost;
import by.nerallan.financialrecords.ui.model.CostMainModel;

public class CostPresenter implements MainContract.MainPresenter,
		MainContract.MainPresenter.OnDataChangeListener {

	private static final String TAG = "CostPresenter";

	private CostMainModel model;
	private MainContract.MainView view;

	public CostPresenter(){
		this.model = new CostMainModel();
		Log.d(TAG, "Constructor");
	}

	@Override
	public void onAttachView(MainContract.MainView view) {
		if (view == null)
			throw new IllegalArgumentException("You can't set a null view");

		this.view = view;
	}

	@Override
	public void onDetachView() {
		/**
		 * Если бы мы работали например с RxJava, в этом классе стоило бы отписываться от подписок
		 * Кроме того, при работе с другими методами асинхронного андроида,здесь мы боремся с утечкой контекста
		 */
		view = null;
		Log.d(TAG, "onDestroy()");
	}

	@Override
	public void onResume() {
		view.showProgress();
		model.loadItems(this);
	}


	@Override
	public void onItemSelected() {
		view.setNoData("SELECTED ITEM");
	}

	@Override
	public void onSuccess(List<Cost> listCost) {
		view.setCollection(listCost);
		view.hideProgress();
	}

	@Override
	public void onFailed(String error) {

	}
}
