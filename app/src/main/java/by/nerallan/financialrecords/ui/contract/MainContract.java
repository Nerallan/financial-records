package by.nerallan.financialrecords.ui.contract;

import java.util.List;

import by.nerallan.financialrecords.ui.model.Cost;

public interface MainContract {
	interface MainView {
		void setCollection(List<Cost> costList);
		void setNoData(String message);
		void showProgress();
		void hideProgress();
	}
	interface MainModel {
		void addItem(Cost cost);
		void removeItem(int id);
		void loadItems(MainPresenter.OnDataChangeListener listener);
		Cost editItem(Cost cost);
	}
	interface MainPresenter {
		interface OnDataChangeListener{
			void onSuccess(List<Cost> listCost);
			void onFailed(String error);
		}
		void onAttachView(MainView view);
		void onDetachView();
		void onResume();
		void onItemSelected();
	}
}
